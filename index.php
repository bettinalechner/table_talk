<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Applications</title>

    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">

</head>
<body ng-app="ApplicationApp">

<section>

<h1>Applications</h1>

<div ng-controller="ApplicationController">
    <table class="table" ng-table="tableParams" show-filter="true">
        <thead>
            <tr>
                <th>
                    Student First
                </th>
                <th>
                    Student Last
                    <i class="fa fa-search"
                        ng-click="showStudentLastName = !showStudentLastName"></i>
                    <i class="fa fa-sort"
                        ng-class="{
                            'fa-sort-asc': tableParams.isSortBy('student_last_name', 'asc'),
                            'fa-sort-desc': tableParams.isSortBy('student_last_name', 'desc')}"
                        ng-click="tableParams.sorting({student_last_name: tableParams.isSortBy('student_last_name', 'asc') ? 'desc' : 'asc'})"></i>
                    <input type="text"
                        ng-model="params.filter()['student_last_name']"
                        ng-show="showStudentLastName"
                        ng-blur="showStudentLastName = hideField($event)"
                        placeholder="Start typing to filter...">
                </th>
                <th>
                    Teacher First
                    <i class="fa fa-search"
                        ng-click="showTeacherFirstName = !showTeacherFirstName"></i>
                    <i class="fa fa-sort"
                        ng-class="{
                            'fa-sort-asc': tableParams.isSortBy('teacher_first_name', 'asc'),
                            'fa-sort-desc': tableParams.isSortBy('teacher_first_name', 'desc')}"
                        ng-click="tableParams.sorting({teacher_first_name: tableParams.isSortBy('teacher_first_name', 'asc') ? 'desc' : 'asc'})"></i>
                    <input type="text"
                        ng-model="params.filter()['teacher_first_name']"
                        ng-show="showTeacherFirstName"
                        ng-blur="showTeacherFirstName = hideField($event)"
                        placeholder="Start typing to filter...">
                </th>
                <th>
                    Teacher Last
                    <i class="fa fa-search"
                        ng-click="showTeacherLastName = !showTeacherLastName"></i>
                    <i class="fa fa-sort"
                        ng-class="{
                            'fa-sort-asc': tableParams.isSortBy('teacher_last_name', 'asc'),
                            'fa-sort-desc': tableParams.isSortBy('teacher_last_name', 'desc')}"
                        ng-click="tableParams.sorting({teacher_last_name: tableParams.isSortBy('teacher_last_name', 'asc') ? 'desc' : 'asc'})"></i>
                    <input type="text"
                        ng-model="params.filter()['teacher_last_name']"
                        ng-show="showTeacherLastName"
                        ng-blur="showTeacherLastName = hideField($event)"
                        placeholder="Start typing to filter...">
                </th>
                <th>
                    Parent Name
                    <i class="fa fa-search"
                        ng-click="showParentNameFilter = !showParentNameFilter"></i>
                    <i class="fa fa-sort"
                        ng-class="{
                            'fa-sort-asc': tableParams.isSortBy('parent_name', 'asc'),
                            'fa-sort-desc': tableParams.isSortBy('parent_name', 'desc')}"
                        ng-click="tableParams.sorting({parent_name: tableParams.isSortBy('parent_name', 'asc') ? 'desc' : 'asc'})"></i>
                    <input type="text"
                        ng-model="params.filter()['parent_name']"
                        ng-show="showParentNameFilter"
                        ng-blur="showParentNameFilter = hideField($event)"
                        placeholder="Start typing to filter...">
                </th>
                <th>
                    School
                    <i class="fa fa-search"
                        ng-click="showSchoolNameFilter = !showSchoolNameFilter"></i>
                    <i class="fa fa-sort"
                        ng-class="{
                            'fa-sort-asc': tableParams.isSortBy('school_name', 'asc'),
                            'fa-sort-desc': tableParams.isSortBy('school_name', 'desc')}"
                        ng-click="tableParams.sorting({school_name: tableParams.isSortBy('school_name', 'asc') ? 'desc' : 'asc'})"></i>
                    <input type="text"
                        ng-model="params.filter()['school_name']"
                        ng-show="showSchoolNameFilter"
                        ng-blur="showSchoolNameFilter = hideField($event)"
                        placeholder="Start typing to filter...">
                </th>
                <th>
                    Application Date
                    <i class="fa fa-sort"
                        ng-class="{
                            'fa-sort-asc': tableParams.isSortBy('created_at', 'asc'),
                            'fa-sort-desc': tableParams.isSortBy('created_at', 'desc')}"
                        ng-click="tableParams.sorting({created_at: tableParams.isSortBy('created_at', 'asc') ? 'desc' : 'asc'})"></i>
                </th>
                <th>
                    Accepted
                    <i class="fa fa-search"
                        ng-click="showAcceptedFilter = !showAcceptedFilter"></i>
                    <select
                        ng-model="params.filter()['accepted']"
                        ng-show="showAcceptedFilter"
                        ng-blur="showAcceptedFilter = hideField($event)">
                            <option></option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                    </select>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="application in tableParams.data">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

    </section>

    <!-- Scripts -->
    <script src="//cdn.rawgit.com/jpillora/xdomain/0.7.4/dist/xdomain.min.js" slave="http://ng-techtalk.herokuapp.com/proxy.html"></script>
    <script src="/js/jquery.js"></script>
    <script src="/js/angular.js"></script>
    <script src="/js/ng-resource.js"></script>
    <script src="/js/ng-table.js"></script>
    <script src="/js/ng-infinite-scroll.js"></script>
    <script src="/js/ng-app.js"></script>

    <script src="/js/controllers/ApplicationController.js"></script>
    <script src="/js/factories/applicationService.js"></script>
</body>
</html>