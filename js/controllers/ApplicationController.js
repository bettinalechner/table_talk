app.controller('ApplicationController', ['$scope', '$location', '$filter', '$resource', 'ngTableParams', 'ApplicationService', '$sce', 
    function($scope, $location, $filter, $resource, ngTableParams, ApplicationService, $sce) {

        $scope.hideField = function(e) {
            if (e.target.value == '') {
                return false;
            } else {
                return true;
            }
        }
    }
]);
