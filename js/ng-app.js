var app = angular.module('ApplicationApp', [
    'ngTable',
    'ngResource',
    'infinite-scroll'
]).filter('boolean', function() {
    return function(input) {
        if (input == 1) {
            return 'Yes';
        } else {
            return 'No';
        }
    }
});